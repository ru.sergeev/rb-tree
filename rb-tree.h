//
// Created by Dmitry Sergeev on 10/2/16.
//

#ifndef KD_TREE_LLRBBST_H
#define KD_TREE_LLRBBST_H

#include "bst.h"
#include "node.h"

template <class Key, class Value> class rbtree : public bst<Key, Value> {
private:
	static const bool RED = true;
	static const bool BLACK = false;

public:
	void put(const Key& key, Value val) override
	{
		bst<Key, Value>::put(key, val);
	}
	void del(const Key& key) override
	{
		bst<Key, Value>::del(key);
	}
protected:
	static bool isRed(const Node<Key, Value>* node) {
		if (node == nullptr) return false;
		return node->color == RED;
	}
	Node<Key, Value>* rotateLeft(Node<Key, Value>* node) {
		assert(isRed(node->right));
		Node<Key, Value>* x = node->right;
		node->right = x->left;
		x->left = node;
		x->color = node->color;
		node->color = RED;
		return x;
	}
	Node<Key, Value>* rotateRight(Node<Key, Value>* node) {
		assert(isRed(node->left));
		Node<Key, Value>* x = node->left;
		node->left = x->right;
		x->right = node;
		x->color = node->color;
		node->color = RED;
		return x;
	}
	void flipColors(const Node<Key, Value>* node) {
		assert(!isRed(node));
		assert(isRed(node->left));
		assert(isRed(node->right));
		node->color = RED;
		node->left->color = BLACK;
		node->right->color = BLACK;
	}
	Node<Key, Value>* fixColors(Node<Key, Value>* node) {
		if (isRed(node->right) && !isRed(node->left)) node = rotateLeft(node);
		if (isRed(node->left) && isRed(node->left->left)) node = rotateRight(node);
		if (isRed(node->left) && isRed(node->right)) flipColors(node);
		return node;
	}

	Node<Key, Value>* put(Node<Key, Value>* node, const Key& key, Value val) override
	{
		if (node == nullptr) return new Node<Key, Value>(key, val, RED);
		node = bst<Key, Value>::put(node, key, val);
		node = fixColors(node);
		return node;
	}

	Node<Key, Value>* removeMin(Node<Key, Value>* node) override
	{
		node = bst<Key, Value>::removeMin(node);
		node = fixColors(node);
		return node;
	}

	Node<Key, Value>* del(Node<Key, Value>* node, const Key& key) override
	{
		node = bst<Key, Value>::del(node, key);
		node = fixColors(node);
		return node;
	}
};


#endif //KD_TREE_LLRBBST_H
