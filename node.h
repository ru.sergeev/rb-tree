#ifndef KD_TREE_NODE_H
#define KD_TREE_NODE_H

template<typename Key, typename Value>
class Node {
public:
	Key key;
	Value val;
	Node(const Key& key, Value val, bool color) : Node(key, val)
	{
		this->color = color;
	}
	Node(const Key& key, Value val) : key(key), val(val) {
		left = nullptr;
		right = nullptr;
	}
	mutable Node* left;
	mutable Node* right;
	mutable bool color;
};

#endif