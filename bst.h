//
// Created by Dmitry Sergeev on 10/1/16.
//

#ifndef KD_TREE_BST_H
#define KD_TREE_BST_H

#include "node.h"
#include <stdexcept>

template <class Key, class Value> class bst {
protected:

	mutable Node<Key, Value>* root;
public:
	bst() :root(nullptr) {}

	virtual void put(const Key& key, Value val) {
		root = put(root, key, val);
	}
	Value get(const Key& key) const {
		auto x = root;
		while (x != nullptr) {
			if (key < x->key) x = x->left;
			else if (key > x->key) x = x->right;
			else if (key == x->key) return x->val;
		}
		throw std::invalid_argument("key not found");
	}

	virtual void del(const Key& key) {
		root = del(root, key);
	}
	Key root_key() const {
		return root->key;
	}
	bool empty() const {
		return root == nullptr;
	}
	virtual ~bst()
	{
		clean(root);
	}
protected:
	virtual Node<Key, Value>* new_node(const Key& key, Value val) {
		return new Node<Key, Value>(key, val);
	}
	static void del(Node<Key, Value>* node) {
		assert(node != nullptr);
		delete node;
	}
	virtual Node<Key, Value>* put(Node<Key, Value>* node, const Key& key, Value val) {
		if (node == nullptr) return new_node(key, val);
		if (key < node->key)
			node->left = put(node->left, key, val);
		else
			node->right = put(node->right, key, val);
		return node;
	}
	virtual Node<Key, Value>* removeMin(Node<Key, Value>* node) {
		if (node->left == nullptr) {
			return node->right;
		}
		node->left = removeMin(node->left);
		return node;
	}
	static Node<Key, Value>* min(Node<Key, Value>* node) {
		while (node->left != nullptr) node = node->left;
		return node;
	}
	virtual Node<Key, Value>* del(Node<Key, Value>* node, const Key& key) {
		if (node == nullptr) return nullptr;
		if (key < node->key) node->left = del(node->left, key);
		else if (key > node->key) node->right = del(node->right, key);
		else {
			if (node->right == nullptr) {
				auto next = node->left;
				del(node);
				return next;
			}
			if (node->left == nullptr) {
				auto next = node->right;
				del(node);
				return next;
			}
			auto t = node;
			node = min(t->right);
			node->right = removeMin(t->right);
			node->left = t->left;
		}
		return node;
	}
private:
	void clean(Node<Key, Value>* node) {
		if (node == nullptr) return;
		clean(node->left);
		clean(node->right);
		del(node);
	}
};

#endif //KD_TREE_BST_H
